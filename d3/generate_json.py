import argparse
import json
import logging
import sys

import pandas as pd
from sbtab import SBtab


def MakeParser():
    parser = argparse.ArgumentParser(
        description="Generate a JSON file with Proteomap data for d3-voronoi-treemap."
    )
    parser.add_argument(
        "sbtab_fname", type=str, help="path to SBtab file containing the Proteome data"
    )

    logging.getLogger().setLevel(logging.WARNING)

    return parser


def main(args):
    """Run main script, generate the JSON output."""

    sbtabdoc = SBtab.read_csv(args.sbtab_fname, "proteome")
    proteomics_df = sbtabdoc.get_sbtab_by_id("VoronoiTreemap").to_data_frame()
    proteomics_df["Weight"] = proteomics_df.Weight.apply(float)
    proteomics_df = proteomics_df[proteomics_df.Weight > 0]

    root = dict()
    root["name"] = "Proteins"
    root["children"] = []

    for gr1, group1_df in proteomics_df.groupby("Level1"):
        subtree1 = dict()
        subtree1["name"] = gr1
        subtree1["color"] = "#AAAAAA"
        subtree1["children"] = []
        for gr2, group2_df in group1_df.groupby("Level2"):
            subtree2 = dict()
            subtree2["name"] = gr2
            subtree2["color"] = "#AAAAAA"
            subtree2["children"] = []
            for gr3, group3_df in group2_df.groupby("Level3"):
                subtree3 = dict()
                subtree3["name"] = gr3
                subtree3["color"] = "#AAAAAA"  # group3_df.Color.iat[0]
                subtree3["children"] = [
                    {"name": row.Name, "weight": row.Weight, "color": row.Color}
                    for row in group3_df.itertuples()
                ]
                subtree2["children"].append(subtree3)
            subtree1["children"].append(subtree2)
        root["children"].append(subtree1)

    sys.stdout.write(json.dumps(root, indent="\t"))


if __name__ == "__main__":
    parser = MakeParser()
    args = parser.parse_args()
    main(args)
