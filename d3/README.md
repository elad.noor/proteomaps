How to use d3-voronoi-treemap to generate Proteomaps
====================================================

First, make sure you have these dependencies installed using pip:
```bash
pip install pandas numpy sbtab
```

The Jupyter notebook `generate_json.ipynb` takes an SBtab file and converts the data into a JSON file.
```bash
python generate_json.py test_output.tsv > voronoi_data.json
```
Note that the file name `voronoi_data.json` is hard-coded into the HTML file, so if you want to change the name, you also need
to go into `index.html` and change it there for the next step to work.

Next, you need to open the `index.html` file through a web server. A simple way of running one is using the python command:
```bash
python -m http.server
```
from the same directory. You should be able to see the proteomap in the browser if you point it to [http://0.0.0.0:8000/](http://0.0.0.0:8000/).


